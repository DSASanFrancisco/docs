# San Francisco DSA Documents

## Bylaws and Code of Conduct

## Proposing Changes

Use the `Open in Web IDE` button to propose changes to the Bylaws and Code of Conduct markdown files.

Note that the table of contents is automatically generated.

## Previewing & Deploying Changes

When proposing changes through a pull request, a preview site will be built and available shortly after submitting your request. 

Additionally, you can generate HTML versions of these markdown documents with the included build script.

Running into issues? Email support@dsasf.org.
