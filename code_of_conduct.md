# DSA San Francisco Code of Conduct

## Table of Contents

<!-- Auto Generated, Do not remove -->

# Purpose

The Democratic Socialists of America San Francisco (DSA SF) is dedicated to providing a harassment-free experience for everyone. We do not tolerate harassment of comrades in any form. This Code of Conduct outlines our expectations for all those who participate in our community, as well as the consequences for unacceptable behavior. We invite everyone to help us create safe and positive experiences for everyone.

# Expected Behavior

-   Participate in an authentic and active way. In doing so, you contribute to the health and longevity of this community.
-   Exercise consideration and respect in your speech and actions.
-   Attempt collaboration before conflict.
-   Refrain from demeaning, discriminatory, or harassing behavior and speech.
-   Please avoid making assumptions. If you aren’t aware of what someone’s gender is, and you can’t figure out what pronoun to use, just ask or look at our name tags, where you may find their name and favourite pronoun!
-   Please ask before touching anyone at our events. It’s easy, and the worst thing that happens is someone says no! That includes hugging – you might not know that it makes some people uncomfortable, but it does, so please ask first.
-   Be mindful of your surroundings and the other participants. Alert community leaders if you notice a dangerous situation, someone in distress, or violations of this Code of Conduct, even if they seem inconsequential.

# Unacceptable Behavior

Unacceptable behaviors include: intimidating, harassing, abusive, discriminatory, intentional misgendering, derogatory or demeaning speech or actions by any participant in our community online, at all related events and in one-on-one communications carried out in the context of community business. Community event venues may be shared with members of the public; please be respectful to all patrons of these locations.

Harassment includes:

-   harmful or prejudicial verbal or written comments related to gender, sexual orientation, transness, physical appearance, body size, technical choices, lack of technical knowledge, ability, ethnicity, socioeconomic status, religion (or lack thereof) and other personal conditions and choices
-   trolling, i.e. sustained disruption of conversations, talks or other events
-   non-consensual photography or recording
-   inappropriate use of nudity and/or sexual images in public spaces (including presentation slides)
-   deliberate intimidation, stalking or following
-   non-consensual physical contact
-   unwelcome sexual attention
-   microaggressions, i.e. small, subtle, often subconscious actions that marginalize people from oppressed groups
-   minimizing other people’s experiences

# Representing DSA SF

The reputation of our chapter is based on socialist ideals and accurate representation of our chapter values. Unacceptable representation includes:

-   claiming to speak on behalf or represent that the chapter without the chapter’s approval
-   reporting on closed meetings or reporting without consent

# Photos/Videos

**Ask _everybody_ in the picture if they agree to be photographed or filmed.**

For group shots, ask if anyone wants to opt-out before taking any, or ask people if they need their face obscured after taking photos.

# What happens in case of violations of our Code of Conduct?

Members and guests asked to stop any hateful or disrespectful behavior are expected to **comply immediately**. If a person insists in such behavior, the administrators may take any action they deem appropriate, including warning or removing them from the event, communication channel, etc.

If you are being treated disrespectfully or harassed, notice that someone else is being treated disrespectfully or harassed, or have any other concerns, please point out the problem to the administrator or any member you feel comfortable talking to as soon as possible.

You can also report any violation here: <https://dsasf.org/request-conflict-resolution/>
For more information about Conflict Resolution please [click here](https://dsasf.org/conflict-resolution/).

# Addressing Grievances

If you feel you have been falsely or unfairly accused of violating this Code of Conduct, you should notify one of the event organizers via email or in person with a concise description of your grievance. Your grievance will be handled in accordance with our existing governing policies.

# Scope

This code of conduct applies to all DSA SF online activities, including mailing lists, Twitter and social media, as well as any future public discussion forums. Anyone who violates this code of conduct may be sanctioned or expelled from these spaces at the discretion of the moderation
team.

# Why a code of conduct

We would prefer to live in a society where we do not need Codes of Conduct. However, Codes of Conduct are essential to establish spaces that are different from – and more inclusive than – general society. If you don’t set up your own rules, you implicitly endorse those prevalent in society – including the unwritten ones – many of which we recognize as unfair to many people. When privileges are not explicitly addressed by the ethos of a space, the burden of education will often be placed upon the people who are living the oppressions. Moreover, since we still perform – consciously or unconsciously – behaviours that have oppressive potential (i.e.patriarchal, racist, sexist, capitalist, (neo)colonialist, etc.), it is essential to reflect on our privileges and on the ways in which they have an impact on our lives and the lives of others.

**A code of conduct can help do just that: to bring awareness, consciousness, reflexivity and ultimately change.**

# Candidates’ Public Affiliation with DSA

DSA SF who are running as candidates for public office but have not received the chapter's endorsement are forbidden from advertising or publicizing their member status in a manner designed to promote their candidacy. Unacceptable behavior includes, but is not limited to, publicizing the candidate's membership in DSA or DSA SF in campaign materials or social media, or in public statements. Candidates may state the fact of their membership in DSA or DSA SF if they do so in a manner that a reasonable person would not view as an attempt to use the fact of their membership to promote their candidacy. For example, a candidate may normally discuss the fact of their membership in private settings and in limited public settings (such as at a DSA meeting or event, or if directly asked about membership), or may list it on a non-public social media profile.

# License and Attribution

This Code of Conduct is distributed under a Creative Commons Attribution-ShareAlike license. It is based on the [Berlin Code of Conduct](http://berlincodeofconduct.org/), and the Central New Mexico DSA code of conduct.
